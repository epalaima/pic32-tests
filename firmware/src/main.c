/*******************************************************************************
  MPLAB Harmony Project Main Source File

  Company:
    Microchip Technology Inc.
  
  File Name:
    main.c

  Summary:
    This file contains the "main" function for an MPLAB Harmony project.

  Description:
    This file contains the "main" function for an MPLAB Harmony project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state 
    machines of all MPLAB Harmony modules in the system and it calls the 
    "SYS_Tasks" function from within a system-wide "super" loop to maintain 
    their correct operation. These two functions are implemented in 
    configuration-specific files (usually "system_init.c" and "system_tasks.c")
    in a configuration-specific folder under the "src/system_config" folder 
    within this project's top-level folder.  An MPLAB Harmony project may have
    more than one configuration, each contained within it's own folder under
    the "system_config" folder.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

//Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "system/common/sys_module.h"   // SYS function prototypes


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************
int toggle = 0;

/*
void __ISR(4, IPL1) _Timer1Handler(void){
    if(toggle = 0){
    LATBbits.LATB2 = 0;
    LATBbits.LATB3 = 0;
    LATBbits.LATB10 = 0; 
    toggle = 1;
    }
    else{
    LATBbits.LATB2 = 1;
    LATBbits.LATB3 = 1;
    LATBbits.LATB10 = 1;   
    toggle = 0;
    }
    IFS0bits.T1IF = 0;
}
*/
int main ( void )
{
    /* Initialize all MPLAB Harmony modules, including application(s). */
    SYS_Initialize ( NULL );
    //TRISBbits.TRISB2 = 1;
    TRISB = 0x00000000000000000000000000000000;
    /*
    IEC0bits.T1IE = 1;
    T1CONbits.ON = 1;
    T1CONbits.TCKPS = 3;
    */
    //IEC0<4> = 1;
    
    unsigned long x = 0;
    
    unsigned long delayTime = 8000000;
    /*
    LATBbits.LATB2 = 1;
    LATBbits.LATB3 = 1;
    LATBbits.LATB10 = 1;
    */

    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        //SYS_Tasks ( );
        LATBbits.LATB2 = 1;
        LATBbits.LATB3 = 1;
        LATBbits.LATB10 = 1;
        while(x < delayTime){
            x++;
        }
        x = 0;
        LATBbits.LATB2 = 0;
        LATBbits.LATB3 = 0;
        LATBbits.LATB10 = 0;
        while(x < delayTime){
            x++;
        }
        x = 0;
        LATBbits.LATB2 = 1;
        LATBbits.LATB3 = 0;
        LATBbits.LATB10 = 0;
        while(x < delayTime){
            x++;
        }
        x = 0;
        LATBbits.LATB2 = 0;
        LATBbits.LATB3 = 1;
        LATBbits.LATB10 = 0;
        while(x < delayTime){
            x++;
        }
        x = 0;
        LATBbits.LATB2 = 0;
        LATBbits.LATB3 = 0;
        LATBbits.LATB10 = 1;
        while(x < delayTime){
            x++;
        }
        x = 0;
        LATBbits.LATB2 = 1;
        LATBbits.LATB3 = 1;
        LATBbits.LATB10 = 0;
        while(x < delayTime){
            x++;
        }
        x = 0;
        LATBbits.LATB2 = 0;
        LATBbits.LATB3 = 1;
        LATBbits.LATB10 = 1;
        while(x < delayTime){
            x++;
        }
        x = 0; 
        LATBbits.LATB2 = 1;
        LATBbits.LATB3 = 0;
        LATBbits.LATB10 = 1;
        while(x < delayTime){
            x++;
        }
        x = 0;  
    }
    

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

